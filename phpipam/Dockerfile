FROM php:5.6-apache
MAINTAINER Roland Stumpner <roland@stumpner.at>

ENV PHPIPAM_SOURCE https://github.com/phpipam/phpipam/archive/
ENV PHPIPAM_VERSION 1.3
ENV WEB_REPO /var/www/html
ENV MYSQL_HOST mariadb
ENV MYSQL_USER root
ENV MYSQL_PASSWORD phpipam

# Install required deb packages
RUN apt-get update && \
	apt-get install -y git php-pear php5-curl php5-mysql php5-json php5-gmp php5-mcrypt php5-ldap libgmp-dev libmcrypt-dev php5-gd php-net-socket libgd3 libpng-dev libldb-dev libldap2-dev ldap-utils libxml2-dev && \
	rm -rf /var/lib/apt/lists/*

# Configure apache and required PHP modules
RUN docker-php-ext-configure mysqli --with-mysqli=mysqlnd && \
	docker-php-ext-install mysqli && \
	docker-php-ext-install pdo_mysql && \
	docker-php-ext-install gettext && \
	docker-php-ext-install gd && \
	docker-php-ext-install sockets && \
	ln -s /usr/include/x86_64-linux-gnu/gmp.h /usr/include/gmp.h && \
	docker-php-ext-configure gmp --with-gmp=/usr/include/x86_64-linux-gnu && \
	docker-php-ext-install gmp && \
        docker-php-ext-install mcrypt && \
        docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/ && \
        docker-php-ext-install ldap && \
        ln -s /usr/lib/x86_64-linux-gnu/libldap.so /usr/lib/libldap.so && \
        ln -s /usr/lib/x86_64-linux-gnu/liblber.so /usr/lib/liblber.so && \
	echo ". /etc/environment" >> /etc/apache2/envvars && \
	a2enmod rewrite

COPY php.ini /usr/local/etc/php/

# copy phpipam sources to web dir
ADD ${PHPIPAM_SOURCE}/${PHPIPAM_VERSION}.tar.gz /tmp/
RUN	tar -xzf /tmp/${PHPIPAM_VERSION}.tar.gz -C ${WEB_REPO}/ --strip-components=1

# Copy the Migration DB to the Docker Container
COPY MIGRATE.sql ${WEB_REPO}/db/

# Copy a Environment Variable Friendly Version of config.php
COPY config.php ${WEB_REPO}/

# Use system environment variables into config.php
#RUN cp ${WEB_REPO}/config.dist.php ${WEB_REPO}/config.php && \
#    sed -i \
#      -e "s/'localhost'/getenv('MYSQL_HOST')/" \
#			-e "s/'phpipamadmin'/getenv('MYSQL_PASSWORD')/"
#     sed -i "s/phpipamadmin/poc-phpipam/"
#    -e "s/\['host'\] = \'localhost\'/\['host'\] = getenv(\"MYSQL_HOST\")/" \
#    -e "s/\$db['user'\] = \"phpipam\"/\$db['user'\] = getenv(\"MYSQL_USER\")/" \
#    -e "s/\['pass'\] = \"phpipamadmin\"/\['pass'\] = getenv(\"MYSQL_PASSWORD\")/" \
#	${WEB_REPO}/config.php

EXPOSE 80
EXPOSE 443
